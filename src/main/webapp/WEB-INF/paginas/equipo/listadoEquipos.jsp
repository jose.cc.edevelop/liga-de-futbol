<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
        <fmt:setLocale value="es_mx" />
        <section id="equipos">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header">
                                <h4>Listado de equipos.</h4>
                            </div>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                <table class="table table-striped table-bordered mb-0">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Direccion</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="equipo" items="${equipos}" varStatus="status">
                                            <tr>
                                                <td scope="row">${status.count}</td>
                                                <td>${equipo.nombre} </td>
                                                <td>${equipo.direccion}</td>
                                                <td>
                                                    <a href="${pageContext.request.contextPath}/ServletControlador?accion=editar&idEquipo=${equipo.idEquipo}"
                                                        class="btn btn-secondary">
                                                        <i class="fas fa-angle-double-right"></i>Editar
                                                    </a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Tarjeta para los totales  -->
                    <div class="col-md-3">
                        <div class="card text-center bg-success text-white mb-3">
                            <div class="card-body">
                                <h3>Total Equipos</h3>
                                <h4 class="display-4">
                                    <i class="fas fa-users"></i><br />${totalEquipos}
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Agregar cliente modal -->
        <jsp:include page="/WEB-INF/paginas/equipo/agregarEquipo.jsp" />