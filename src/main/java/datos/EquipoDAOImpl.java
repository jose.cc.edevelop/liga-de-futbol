package datos;

import dominio.Equipo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EquipoDAOImpl implements IEquipoDAO {
    
    private static final String SQL_SELECT = "SELECT ID_Equipo, nombre, telefono, direccion "
            + "FROM equipos;";
    private static final String SQL_SELECT_BY_ID = "SELECT ID_Equipo, nombre, telefono, direccion "
            + "FROM equipos WHERE ID_Equipo = ?;";
    private static final String SQL_INSERT = "INSERT INTO equipos (ID_Equipo, nombre, telefono, direccion) "
            + "Values( ?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE equipos SET "
            + "nombre = ?, telefono = ?, direccion = ? WHERE ID_Equipo = ?";
    private static final String SQL_DELETE = "DELETE FROM equipos WHERE ID_Equipo = ?";
    private static final String SQL_OBTENER_ID = "SELECT count(ID_Equipo) + 1 AS id FROM equipos";

    @Override
    public int insertar(Equipo equipo) {
        int registrosInsertados = 0;
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = Conexion.getConnection();
            pstmt = conn.prepareStatement(SQL_INSERT);
            pstmt.setInt(1, this.obtenerID());
            pstmt.setString(2, equipo.getNombre());
            pstmt.setString(3, equipo.getTelefono());
            pstmt.setString(4, equipo.getDireccion());
            registrosInsertados = pstmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally
        {
            Conexion.close(pstmt);
            Conexion.close(conn);
        }
        
        return registrosInsertados;
    }

    @Override
    public int actualizar(Equipo equipo) {
        int registrosActualizados = 0;
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = Conexion.getConnection();
            pstmt = conn.prepareStatement(SQL_UPDATE);
            pstmt.setString(1, equipo.getNombre());
            pstmt.setString(2, equipo.getTelefono());
            pstmt.setString(3, equipo.getDireccion());
            pstmt.setInt(4, equipo.getIdEquipo());
            registrosActualizados = pstmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally
        {
            Conexion.close(pstmt);
            Conexion.close(conn);
        }
        return registrosActualizados;
    }

    @Override
    public int eliminar(Equipo equipo) {
        int registrosEliminados = 0;
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = Conexion.getConnection();
            pstmt = conn.prepareStatement(SQL_DELETE);
            pstmt.setInt(1, equipo.getIdEquipo());
            registrosEliminados = pstmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally
        {
            Conexion.close(pstmt);
            Conexion.close(conn);
        }
        return registrosEliminados;
    }

    @Override
    public List<Equipo> listar() {
        List<Equipo> equipos = new ArrayList<>();
        Equipo equipo;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = Conexion.getConnection();
            pstmt = conn.prepareStatement(SQL_SELECT);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                int idEquipo = rs.getInt("id_equipo");
                String nombre = rs.getString("nombre");
                String telefono = rs.getString("telefono");
                String direccion = rs.getString("direccion");
                equipo = new Equipo(idEquipo, nombre, telefono, direccion);
                equipos.add(equipo);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Conexion.close(rs);
            Conexion.close(pstmt);
            Conexion.close(conn);
        }
        return equipos;
    }

    @Override
    public Equipo existe(Equipo equipo) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = Conexion.getConnection();
            pstmt = conn.prepareStatement(SQL_SELECT_BY_ID);
            pstmt.setInt(1, equipo.getIdEquipo());
            rs = pstmt.executeQuery();
            //rs.absolute(1); - Esta linea dejó de funcionar
            rs.next();
            String nombre = rs.getString("nombre");
            String telefono = rs.getString("telefono");
            String direccion = rs.getString("direccion");
            equipo.setNombre(nombre);
            equipo.setTelefono(telefono);
            equipo.setDireccion(direccion);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Conexion.close(rs);
            Conexion.close(pstmt);
            Conexion.close(conn);
        }
        return equipo;
    }

    @Override
    public int obtenerID() {
        int id = 0;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try{
            conn = Conexion.getConnection();
            pstmt = conn.prepareStatement(SQL_OBTENER_ID);
            rs = pstmt.executeQuery();
            rs.next();
            id = rs.getInt("id");
        }
        catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally {
            Conexion.close(rs);
            Conexion.close(pstmt);
            Conexion.close(conn);
        }
        return id;
    }
    
}
