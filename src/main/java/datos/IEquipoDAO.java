/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Equipo;
import java.util.List;

/**
 *
 * @author joseeduardo
 */
public interface IEquipoDAO  {
    
    int insertar(Equipo equipo);
    int actualizar(Equipo equipo);
    int eliminar(Equipo equipo);
    List<Equipo> listar();
    Equipo existe(Equipo equipo);
    int obtenerID();
    
}
