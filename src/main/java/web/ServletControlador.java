package web;

import datos.EquipoDAOImpl;
import datos.IEquipoDAO;
import dominio.Equipo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/ServletControlador")
public class ServletControlador extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String accion = request.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "editar":
                    this.editarEquipo(request, response);
                    break;
                case "eliminar":
                    this.eliminarEquipo(request, response);
                    break;
                default:
                    this.accionDefault(request, response);
            }
        }
        else {
            this.accionDefault(request, response);
        }
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "insertar":
                    this.insertarEquipo(request, response);
                    break;
                case "modificar":
                    this.modificarEquipo(request, response);
                    break;
                    
                default:
                    this.accionDefault(request, response);
            }
        }
        else {
            this.accionDefault(request, response);
        }
    }
    
    private void accionDefault(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Equipo> equipos = new ArrayList<>();
        IEquipoDAO equipo = new EquipoDAOImpl();
        equipos = equipo.listar();
        System.out.println("Equipos: " + equipos);
        HttpSession session = request.getSession();
        session.setAttribute("equipos", equipos);
        session.setAttribute("totalEquipos", equipos.size());
        //request.getRequestDispatcher("clientes.jsp").forward(request, response);
        response.sendRedirect("equipos.jsp");
    }

    /***
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void insertarEquipo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int equiposRegistrados = 0;
        String nombre = request.getParameter("nombre");
        String telefono = request.getParameter("telefono");
        String direccion = request.getParameter("direccion");
        Equipo equipo = new Equipo(nombre, telefono, direccion);
        IEquipoDAO equipoDAO = new EquipoDAOImpl();
        equiposRegistrados = equipoDAO.insertar(equipo);
        System.out.println("Total de equipos registrados: " + equiposRegistrados);
        this.accionDefault(request, response);
    }

    /***
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void modificarEquipo(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        
        int idEquipo = Integer.parseInt(request.getParameter("idEquipo"));
        String nombre = request.getParameter("nombre");
        String telefono = request.getParameter("telefono");
        String direccion = request.getParameter("direccion");
        Equipo equipo = new Equipo(idEquipo, nombre, telefono, direccion);
        IEquipoDAO equipoDao = new EquipoDAOImpl();
        int clientesActualizados = equipoDao.actualizar(equipo);
        System.out.println("Clientes actualizados: " + clientesActualizados);
        this.accionDefault(request, response);
        
    }

    private void editarEquipo(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        // recuperamos el id cliente.
        int idCliente = Integer.parseInt(request.getParameter("idEquipo"));
        Equipo equipo = new Equipo(idCliente);
        IEquipoDAO equipoDao = new EquipoDAOImpl();
        equipo = equipoDao.existe(equipo);
        request.setAttribute("equipo", equipo);
        String jspEditar = "/WEB-INF/paginas/equipo/editarEquipo.jsp";
        request.getRequestDispatcher(jspEditar).forward(request, response);
    }

    private void eliminarEquipo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int idEquipo = Integer.parseInt(request.getParameter("idEquipo"));
        Equipo equipo = new Equipo(idEquipo);
        IEquipoDAO equipoDao = new EquipoDAOImpl();
        int clientesEliminados = equipoDao.eliminar(equipo);
        System.out.println("Clientes eliminados: " + clientesEliminados);
        this.accionDefault(request, response);
    
    }

}
